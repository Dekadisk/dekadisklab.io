---
layout: post
title:  "Hyperspeed Rabbit - Trailer available"
date:   2021-02-18 13:57:14 -0300
categories: "video_games"
img: Thumbnail_HS.png
---
A trailer for Hyperspeed Rabbit is now available on Youtube. No lies here, only in-engine footage!

I am part of the team that worked on it, primarily as a **game designer** and **engine/gameplay programmer**. Hyperspeed Rabbit was developed using DirectX and PhysX only - it uses **our own engine**!
The project can be downloaded on the team's official Gitlab, here : http://cpc.cx/tqQ

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Xic0pLmI7JU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
