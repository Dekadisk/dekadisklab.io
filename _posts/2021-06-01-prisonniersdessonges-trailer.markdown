---
layout: post
title:  "Prisonniers des Songes"
date:   2021-06-01 14:01:01 -0300
categories: "video_games"
img: PDS_Thumbnail.png
---
A trailer for Prisonniers des Songes is now available on Youtube (only in french, though, sorry). I am part of the team that worked on it, primarily as the **game designer (technical designer) and gameplay programmer**, and we put in a lot of effort to make it happen.

_The creation of this game involved the utilization of C++ programming and scripting with blueprints in Unreal Engine 4, as well as the incorporation of Wwise for audio functions._

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/-1EHMtNWxX8?t=62" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


