---
layout: post
title:  "Black Fountain - A choice-based text adventure"
date:   2022-11-29 13:18:01 -0300
categories: "video_games"
img: BF_Thumbnail.png
---
I'm working on Black Fountain, a choice-based text adventure (which will be available in French and English). I hope to have it done by the end of june, but for now, here are some early screenshots:

![Black Fountain - early screenshot 1](https://dekadisk.gitlab.io/assets/Screenshot_1.png)
![Black Fountain - early screenshot 2](https://dekadisk.gitlab.io/assets/BF_Screenshot.png)
